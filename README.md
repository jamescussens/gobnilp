FOR INFORMATION ON THE PYTHON IMPLEMENTATION OF GOBNILP PLEASE GO TO
https://bitbucket.org/jamescussens/pygobnilp
THE INFORMATION IN THIS FILE ONLY CONCERNS THE C IMPLEMENTATION.

GOBNILP is a C program which uses [SCIP](https://www.scipopt.org/) to
learn Bayesian network structure from complete discrete data (or
precomputed local scores). GOBNILP was written by [James
Cussens](https://jcussens.github.io) and [Mark
Bartlett](https://rgu-repository.worktribe.com/person/184963/mark-bartlett).

If the BLAS, LAPACK and LAPACKE libraries are available it can also
learn Gaussian BNs from continuous data (see below for more details on
that). The version of GOBNILP that exists in this git repo is only
guaranteed to work with SCIP 9.0.0 (but is likely to work with earlier
versions).

When installing SCIP 9.0.0 install it using "make" rather than
"cmake". Let us suppose you have installed SCIP in the directory
~/local/src/scipoptsuite-9.0.0/scip. Here is how to install GOBNILP to
learn only from discrete data. Just do:


```
git clone https://bitbucket.org/jamescussens/gobnilp.git
cd gobnilp/
./configure.sh ~/local/src/scipoptsuite-9.0.0/scip
make
```

The 'configure.sh' script mentioned just above copies (and alters)
files from SCIP which cannot be included in this repo for licensing
reasons. If you have CPLEX installed (and accessible to SCIP) be sure
to do 'make LPS=cpx' instead of plain 'make' since this will lead to
considerably faster solving. Similarly if you have Gurobi installed
(and accessible to SCIP) do 'make LPS=grb'. See the SCIP documentation
on how to link SCIP to CPLEX and/or Gurobi.

After "make" has run you should have an executable "bin/gobnilp". You
can now run gobnilp on the supplied data file "asia_100.dat". Just do

```
bin/gobnilp data/asia_100.dat
```

This should produce output similar to the following:

```
GOBNILP version development [GitHash: unknown ]
Solving the BN structure learning problem using SCIP.

SCIP version 9.0.0 [precision: 8 byte] [memory: block] [mode: optimized] [LP solver: Soplex 7.0.0] [GitHash: 7205bedd94]
Copyright (c) 2002-2024 Zuse Institute Berlin (ZIB)

Reading parameter file <gobnilp.set>.
File name:		data/asia_100.dat
Problem name:		asia_100
presolving (3 rounds: 3 fast, 2 medium, 1 exhaustive):
 29 deleted vars, 25 deleted constraints, 0 added constraints, 0 tightened bounds, 0 added holes, 16 changed sides, 16 changed coefficients
 0 implications, 103 cliques
presolved problem has 48 variables (48 bin, 0 int, 0 impl, 0 cont) and 47 constraints

time | Best Network Found So Far |   Best Network Possible   |mem/heur|  gap   | compl. |objleav|infleav
 0.0s|       -2.472841e+02       |       -1.119828e+02       |  clique| 120.82%| unknown|     0 |     0 
 0.0s|       -2.456443e+02       |       -2.277916e+02       |   sinks|   7.84%| unknown|     0 |     0 
 0.0s|       -2.456443e+02       |       -2.455002e+02       |  1472k |   0.06%| unknown|     0 |     0 

SCIP Status        : problem is solved [optimal solution found]
Solving Time (sec) : 0.04
Solving Nodes      : 51
Primal Bound       : -2.45644265388371e+02 (3 solutions)
Dual Bound         : -2.45644265388371e+02
Gap                : 0.00 %
Smoking<-Bronchitis, -65.918644
Cancer<-Tuberculosis,TbOrCa, -2.247729
Bronchitis<-Dyspnea, -52.010149
VisitAsia<-Tuberculosis, -12.214946
Tuberculosis<- -2.876200
TbOrCa<-Smoking,Tuberculosis, -21.675646
XRay<-TbOrCa, -16.935375
Dyspnea<- -71.765576
BN score is -245.644265
```

To learn Gaussian networks GOBNILP uses BGe scoring to compute local
scores using the BLAS, LAPACK and LAPACKE libraries. We have found
installing OpenBLAS from source the easiest option (we have only
tried with Linux). Note that OpenBLAS includes LAPACK and LAPACKE
which is particularly convenient.

First go to https://www.openblas.net/ and download the source. At time
of writing this gets you the file OpenBLAS-0.3.26.tar.gz. To install
OpenBLAS successfully you will need a Fortran compiler, such as
gfortran, on your system. (If you are using Ubuntu then "sudo apt-get
install gfortran" should work.) To install the libraries in, say,
"/home/james/local/" do the following:

```
tar zxvf OpenBLAS-0.3.26.tar.gz
cd OpenBLAS-0.3.26
make
make install PREFIX=/home/james/local
```

This will put the header file lapacke.h into /home/james/local/include
and libopenblas.a, the library itself, into
/home/james/local/lib. Obviously replace "/home/james/local/" with
whatever directory is convenient for you. Once you have done all this
you can install GOBNILP as follows:

```
git clone https://bitbucket.org/jamescussens/gobnilp.git
cd gobnilp/
./configure.sh ~/local/src/scipoptsuite-9.0.0/scip /home/james/local
make
```

where again, of course, you can replace "/home/james/local/" with a
directory of your choice. Running configure.sh alters the GOBNILP
Makefile to work with OpenBLAS installed as above. If you are using a
Mac you may have problems running configure.sh since it uses 'sed' to
edit the Makefile which can fail on Macs. In this case just edit the
Makefile yourself to set the variable BLAS to TRUE and the variable
BLASDIR to your equivalent of "/home/james/local/", e.g. just add two
lines like this somewhere near the top of Makefile:

```
BLAS = TRUE
BLASDIR = /home/james/local
```

To check that BGe scoring is working OK, create a file called
"gobnilp.set" in your current working directory and put the following
two lines into it:

```
gobnilp/scoring/continuous = TRUE
gobnilp/scoring/score_type = "BGe"
```

and then

```
bin/gobnilp data/gaussian.dat
```

should produce output similar to: 

```
GOBNILP version development [GitHash: unknown ]
Solving the BN structure learning problem using SCIP.

SCIP version 9.0.0 [precision: 8 byte] [memory: block] [mode: optimized] [LP solver: Soplex 7.0.0] [GitHash: 7205bedd94]
Copyright (c) 2002-2024 Zuse Institute Berlin (ZIB)

Reading parameter file <gobnilp.set>.
File name:		data/gaussian.dat
Problem name:		gaussian
presolving (5 rounds: 5 fast, 3 medium, 2 exhaustive):
 23 deleted vars, 16 deleted constraints, 0 added constraints, 0 tightened bounds, 0 added holes, 21 changed sides, 21 changed coefficients
 0 implications, 885 cliques
presolved problem has 180 variables (180 bin, 0 int, 0 impl, 0 cont) and 142 constraints

time | Best Network Found So Far |   Best Network Possible   |mem/heur|  gap   | compl. |objleav|infleav
 0.0s|       -5.418213e+04       |        3.603185e+03       |  clique|    Inf | unknown|     0 |     0 
 0.0s|       -5.408522e+04       |       -4.174489e+04       |   sinks|  29.56%| unknown|     0 |     0 
 0.2s|       -5.408019e+04       |       -5.287046e+04       |   sinks|   2.29%| unknown|     0 |     0 
 0.7s|       -5.408019e+04       |       -5.315716e+04       |    69M |   1.74%| unknown|     0 |     0 
 0.8s|       -5.405241e+04       |       -5.402703e+04       |    LP  |   0.05%|  79.96%|     2 |     0 

SCIP Status        : problem is solved [optimal solution found]
Solving Time (sec) : 0.86
Solving Nodes      : 29
Primal Bound       : -5.40524108134464e+04 (24 solutions)
Dual Bound         : -5.40524108134464e+04
Gap                : 0.00 %
A<- -7124.782937
B<-D, 487.269556
C<-A,B, -3743.043566
D<- -14692.560411
E<-C,F,G, -7312.558564
F<-A,D,G, -11136.569374
G<- -10530.165518
BN score is -54052.410813
```

BGe scoring was implemented by Sam Vozza as a final-year project in
the Dept of Computer Science, University of York.

The manual can be found (as LaTeX source) in the manual directory.


