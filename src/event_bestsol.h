/* This file was created via small edits to the examples/Eventhdlr/src/event_bestsol.h example that comes with SCIP */

/**@file   examples/Eventhdlr/src/event_bestsol.h
 * @brief  eventhdlr for best solution found
 * @author James Cussens
 */

#include "scip/scip.h"

/** includes event handler for best solution found */
SCIP_RETCODE SCIPincludeEventHdlrBestsol(
   SCIP*                 scip                /**< SCIP data structure */
   );
