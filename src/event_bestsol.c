/* This file was created via small edits to the examples/Eventhdlr/src/event_bestsol.c example that comes with SCIP */

/**@brief  eventhdlr for best solution found
 * @author James Cussens
 */

#include "event_bestsol.h"

#include <string.h>
#include "output.h"
#include "parent_set_data.h"
#include "model_averaging.h"
#include "metadata.h"

#define EVENTHDLR_NAME         "bestsol"
#define EVENTHDLR_DESC         "event handler for best solutions found"


/** initialization method of event handler (called after problem was transformed) */
static
SCIP_DECL_EVENTINIT(eventInitBestsol)
{  /*lint --e{715}*/

   SCIP_Bool writebestsols;
   
   assert(scip != NULL);
   assert(eventhdlr != NULL);
   assert(strcmp(SCIPeventhdlrGetName(eventhdlr), EVENTHDLR_NAME) == 0);

   SCIP_CALL( SCIPgetBoolParam(scip, "gobnilp/writebestsols", &writebestsols) );

   /* notify SCIP that your event handler wants to react on the event type best solution found */
   if( writebestsols )
      SCIP_CALL( SCIPcatchEvent( scip, SCIP_EVENTTYPE_BESTSOLFOUND, eventhdlr, NULL, NULL) );

   return SCIP_OKAY;
}


/** execution method of event handler */
static
SCIP_DECL_EVENTEXEC(eventExecBestsol)
{  /*lint --e{715}*/
   /* SCIP_SOL* bestsol; */
   /* SCIP_Real solvalue; */
   MA_info ma_info;
   ParentSetData* psd;


   assert(eventhdlr != NULL);
   assert(strcmp(SCIPeventhdlrGetName(eventhdlr), EVENTHDLR_NAME) == 0);
   assert(event != NULL);
   assert(scip != NULL);
   assert(SCIPeventGetType(event) == SCIP_EVENTTYPE_BESTSOLFOUND);

   SCIPdebugMsg(scip, "exec method of event handler for best solution found\n");
   
   /* bestsol = SCIPgetBestSol(scip); */
   /* assert(bestsol != NULL); */
   /* solvalue = SCIPgetSolOrigObj(scip, bestsol); */
   
   /* print best solution value */
   /*SCIPinfoMessage(scip, NULL, "found new best solution with solution value <%g> in SCIP <%s>\n", 
     solvalue, SCIPgetProbName(scip) );*/

   psd = MD_getParentSetData(scip);

   /* only call on main SCIP instance which has parent set data attached */
   if( psd != NULL )
      SCIP_CALL( IO_doIterativePrint(scip, &ma_info, psd, 1) );
   
   return SCIP_OKAY;
}

/** includes event handler for best solution found */
SCIP_RETCODE SCIPincludeEventHdlrBestsol(
   SCIP*                 scip                /**< SCIP data structure */
   )
{
   SCIP_EVENTHDLRDATA* eventhdlrdata;
   SCIP_EVENTHDLR* eventhdlr;
   eventhdlrdata = NULL;
   
   eventhdlr = NULL;
   /* create event handler for events on watched variables */
   SCIP_CALL( SCIPincludeEventhdlrBasic(scip, &eventhdlr, EVENTHDLR_NAME, EVENTHDLR_DESC, eventExecBestsol, eventhdlrdata) );
   assert(eventhdlr != NULL);

   /* no copy callback */
   /* SCIP_CALL( SCIPsetEventhdlrCopy(scip, eventhdlr, eventCopyBestsol) ); */
   SCIP_CALL( SCIPsetEventhdlrInit(scip, eventhdlr, eventInitBestsol) );
   /* don't bother with an exit callback */
   /* SCIP_CALL( SCIPsetEventhdlrExit(scip, eventhdlr, eventExitBestsol) ); */
   
   return SCIP_OKAY;
}
